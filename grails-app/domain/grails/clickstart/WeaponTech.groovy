package grails.clickstart

class WeaponTech {
	String name
	Group group
	int cost = 2
	Tier tier
	String prerequisite
	Stacking stacking
	String text
	String quote
	
		
    static constraints = {
	name blank:false
	group blank:false
	cost min:2, blank:false
	tier blank:false
	prerequisite blank:false
	stacking blank:false
	text blank:false, size:0..2000
		
    }
	public enum Stacking {
		ACTIVE, PASSIVE, CRIT
	}
	public enum Tier {
		BEGINNER, ADVANCED, EXPERT, MASTER
	}

	public enum Group {
		HEAVY_AXE_AND_MACE, LIGHT_AXE_AND_MACE, BOW, CROSSBOW, DAGGER, SCYTHE, SHIELD, SPEAR, HEAVY_SWORD, LIGHT_SWORD, UNARMED, WHIP, MOUNTED, FLIGHT
	}

		
}
