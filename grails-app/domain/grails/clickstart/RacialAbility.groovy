package grails.clickstart

class RacialAbility {
	String name
	int cost
	int rank
	String text
	

		
    static constraints = {
	name blank:false
	cost blank:false, min:0, max:2
	rank blank:false, min:1, max:21
	text blank:false, size:0..2000
	}
}
