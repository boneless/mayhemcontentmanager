package grails.clickstart

class AttributeAbility {
	String name
	Attribute attribute
	int cost = 2
	int prerequisite
	Stacking stacking
	String text
	String quote
	
    static constraints = {
	name blank:false
	attribute blank:false
	prerequisite blank:false
	cost blank:false, min:2
	stacking blank:false
	text blank:false, size:0..2000
		    }
	
	public enum Attribute {
		STRENGTH, AGILITY, INTELLIGENCE, INTUITION, ENDURANCE, WILLPOWER, CHARISMA, CUNNING, GENERAL
	}

	public enum Stacking {
		ACTIVE, PASSIVE, CRIT
	}

	
}
