package grails.clickstart

class ElementalSpell {
	String name
	Element element
	Circle circle
	Speed speed
	String resist
	String duration
	int range
	int cost
	String text
	String quote
	
	static constraints = {
	name blank:false
	element blank:false
	circle blank:false
	cost blank:false, min:1
	speed blank:false
	range blank:false
	text blank:false, size:0..2000
	    }

	public enum Element {
		FIRE, ICE, EARTH, WIND, PLANT, VOID
	}	

	public enum Circle {
		FIRST, SECOND, THIRD
	}

	public enum Speed {
		FREE, INSTANT, VERY_FAST, FAST, AVERAGE, SLOW, VERY_SLOW, FULL_ROUND
	}

		
}
