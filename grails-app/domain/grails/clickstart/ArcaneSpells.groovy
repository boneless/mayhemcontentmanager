package grails.clickstart

class ArcaneSpells {
	String name
	School school
	Circle circle
	Speed speed
	String resist
	String duration
	int range
	String text
	String quote

	static constraints = {
		name blank:false
		school blank:false
		circle blank:false
		range blank:false, min:0
		text blank:false, size:0..2000
	}

	public enum School {
		CHRONOMANCY, COMPULSION, DIVINATION, ENHANCEMENT, HEALING, HEXING, ILLUSION, NECROMANCY, RUIN, SUMMONING, TRANSMUTATION, WARDING
	}

	public enum Circle {
		FIRST(1), SECOND(2), THIRD(3), FOURTH(4), FIFTH(5)
		int cost

		public Circle (int cost){
			this.cost = cost
		}
	}


	public enum Speed {
		FREE(0), INSTANT(1), VERY_FAST(5), FAST(6), AVERAGE(7), SLOW(8), VERY_SLOW(9), FULL_ROUND(12)
		int speedValue

		public Speed (int speedValue){
			this.speedValue = speedValue
		}
	}
}
