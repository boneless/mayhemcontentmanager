package grails.clickstart

class Weapon {
	String name
	String weaponStyle
	String attackSkill
	int hands
	Integer meleeRange
	Integer thrownShotRange
	int parry
	int critValue
	int dieNumber
	int dieSides
	String speed
	String reloadSpeed
	int movePenalty
	String requiredSkill
	int skillRequirementValue
	int cost
	
		
    static constraints = {
		name blank:false
		weaponStyle blank:false
		attackSkill blank:false
		hands max:2, min:1, blank:false
		meleeRange max:5, min:1
		thrownShotRange max:25, min:1 
				
    }
}
