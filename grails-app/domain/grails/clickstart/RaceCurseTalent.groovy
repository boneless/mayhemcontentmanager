package grails.clickstart

class RaceCurseTalent {
	String name
	int cost
	AbilityGroup abilityGroup
	AbilityType abilityType
	InhumanAttribute inhumanAttributeA
	InhumanAttribute inhumanAttributeB
	String typeIncompatability
	String description
	String quote
			
	static constraints = {
	name blank:false
	abilityGroup blank:false
	abilityType blank:false
	cost blank:false
	inhumanAttributeA blank:false
	inhumanAttributeB blank:false
	typeIncompatability blank:false
	description blank:false, size:0..2000
	    }
	
	public enum  InhumanAttribute {
		STRENGTH, AGILITY, INTELLIGENCE, INTUITION, ENDURANCE, WILLPOWER, CHARISMA, CUNNING
	}

	public enum  AbilityGroup {
		ANIMAL, DEMONIC, DIVINE, FAE, SHAPESHIFTER, UNDEAD, PSYCHIC, DRAKE, KAMI
	}
	
	public enum  AbilityType {
		RACE, CURSE, TALENT
	}

}
