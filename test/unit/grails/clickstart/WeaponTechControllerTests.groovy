package grails.clickstart



import org.junit.*
import grails.test.mixin.*

@TestFor(WeaponTechController)
@Mock(WeaponTech)
class WeaponTechControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/weaponTech/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.weaponTechInstanceList.size() == 0
        assert model.weaponTechInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.weaponTechInstance != null
    }

    void testSave() {
        controller.save()

        assert model.weaponTechInstance != null
        assert view == '/weaponTech/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/weaponTech/show/1'
        assert controller.flash.message != null
        assert WeaponTech.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/weaponTech/list'

        populateValidParams(params)
        def weaponTech = new WeaponTech(params)

        assert weaponTech.save() != null

        params.id = weaponTech.id

        def model = controller.show()

        assert model.weaponTechInstance == weaponTech
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/weaponTech/list'

        populateValidParams(params)
        def weaponTech = new WeaponTech(params)

        assert weaponTech.save() != null

        params.id = weaponTech.id

        def model = controller.edit()

        assert model.weaponTechInstance == weaponTech
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/weaponTech/list'

        response.reset()

        populateValidParams(params)
        def weaponTech = new WeaponTech(params)

        assert weaponTech.save() != null

        // test invalid parameters in update
        params.id = weaponTech.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/weaponTech/edit"
        assert model.weaponTechInstance != null

        weaponTech.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/weaponTech/show/$weaponTech.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        weaponTech.clearErrors()

        populateValidParams(params)
        params.id = weaponTech.id
        params.version = -1
        controller.update()

        assert view == "/weaponTech/edit"
        assert model.weaponTechInstance != null
        assert model.weaponTechInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/weaponTech/list'

        response.reset()

        populateValidParams(params)
        def weaponTech = new WeaponTech(params)

        assert weaponTech.save() != null
        assert WeaponTech.count() == 1

        params.id = weaponTech.id

        controller.delete()

        assert WeaponTech.count() == 0
        assert WeaponTech.get(weaponTech.id) == null
        assert response.redirectedUrl == '/weaponTech/list'
    }
}
