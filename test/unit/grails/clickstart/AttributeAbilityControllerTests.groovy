package grails.clickstart



import org.junit.*
import grails.test.mixin.*

@TestFor(AttributeAbilityController)
@Mock(AttributeAbility)
class AttributeAbilityControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/attributeAbility/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.attributeAbilityInstanceList.size() == 0
        assert model.attributeAbilityInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.attributeAbilityInstance != null
    }

    void testSave() {
        controller.save()

        assert model.attributeAbilityInstance != null
        assert view == '/attributeAbility/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/attributeAbility/show/1'
        assert controller.flash.message != null
        assert AttributeAbility.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/attributeAbility/list'

        populateValidParams(params)
        def attributeAbility = new AttributeAbility(params)

        assert attributeAbility.save() != null

        params.id = attributeAbility.id

        def model = controller.show()

        assert model.attributeAbilityInstance == attributeAbility
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/attributeAbility/list'

        populateValidParams(params)
        def attributeAbility = new AttributeAbility(params)

        assert attributeAbility.save() != null

        params.id = attributeAbility.id

        def model = controller.edit()

        assert model.attributeAbilityInstance == attributeAbility
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/attributeAbility/list'

        response.reset()

        populateValidParams(params)
        def attributeAbility = new AttributeAbility(params)

        assert attributeAbility.save() != null

        // test invalid parameters in update
        params.id = attributeAbility.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/attributeAbility/edit"
        assert model.attributeAbilityInstance != null

        attributeAbility.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/attributeAbility/show/$attributeAbility.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        attributeAbility.clearErrors()

        populateValidParams(params)
        params.id = attributeAbility.id
        params.version = -1
        controller.update()

        assert view == "/attributeAbility/edit"
        assert model.attributeAbilityInstance != null
        assert model.attributeAbilityInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/attributeAbility/list'

        response.reset()

        populateValidParams(params)
        def attributeAbility = new AttributeAbility(params)

        assert attributeAbility.save() != null
        assert AttributeAbility.count() == 1

        params.id = attributeAbility.id

        controller.delete()

        assert AttributeAbility.count() == 0
        assert AttributeAbility.get(attributeAbility.id) == null
        assert response.redirectedUrl == '/attributeAbility/list'
    }
}
