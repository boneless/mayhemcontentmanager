package grails.clickstart



import org.junit.*
import grails.test.mixin.*

@TestFor(ArcaneSpellsController)
@Mock(ArcaneSpells)
class ArcaneSpellsControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/arcaneSpells/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.arcaneSpellsInstanceList.size() == 0
        assert model.arcaneSpellsInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.arcaneSpellsInstance != null
    }

    void testSave() {
        controller.save()

        assert model.arcaneSpellsInstance != null
        assert view == '/arcaneSpells/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/arcaneSpells/show/1'
        assert controller.flash.message != null
        assert ArcaneSpells.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/arcaneSpells/list'

        populateValidParams(params)
        def arcaneSpells = new ArcaneSpells(params)

        assert arcaneSpells.save() != null

        params.id = arcaneSpells.id

        def model = controller.show()

        assert model.arcaneSpellsInstance == arcaneSpells
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/arcaneSpells/list'

        populateValidParams(params)
        def arcaneSpells = new ArcaneSpells(params)

        assert arcaneSpells.save() != null

        params.id = arcaneSpells.id

        def model = controller.edit()

        assert model.arcaneSpellsInstance == arcaneSpells
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/arcaneSpells/list'

        response.reset()

        populateValidParams(params)
        def arcaneSpells = new ArcaneSpells(params)

        assert arcaneSpells.save() != null

        // test invalid parameters in update
        params.id = arcaneSpells.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/arcaneSpells/edit"
        assert model.arcaneSpellsInstance != null

        arcaneSpells.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/arcaneSpells/show/$arcaneSpells.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        arcaneSpells.clearErrors()

        populateValidParams(params)
        params.id = arcaneSpells.id
        params.version = -1
        controller.update()

        assert view == "/arcaneSpells/edit"
        assert model.arcaneSpellsInstance != null
        assert model.arcaneSpellsInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/arcaneSpells/list'

        response.reset()

        populateValidParams(params)
        def arcaneSpells = new ArcaneSpells(params)

        assert arcaneSpells.save() != null
        assert ArcaneSpells.count() == 1

        params.id = arcaneSpells.id

        controller.delete()

        assert ArcaneSpells.count() == 0
        assert ArcaneSpells.get(arcaneSpells.id) == null
        assert response.redirectedUrl == '/arcaneSpells/list'
    }
}
