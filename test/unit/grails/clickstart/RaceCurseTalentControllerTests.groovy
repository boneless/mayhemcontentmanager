package grails.clickstart



import org.junit.*
import grails.test.mixin.*

@TestFor(RaceCurseTalentController)
@Mock(RaceCurseTalent)
class RaceCurseTalentControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/raceCurseTalent/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.raceCurseTalentInstanceList.size() == 0
        assert model.raceCurseTalentInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.raceCurseTalentInstance != null
    }

    void testSave() {
        controller.save()

        assert model.raceCurseTalentInstance != null
        assert view == '/raceCurseTalent/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/raceCurseTalent/show/1'
        assert controller.flash.message != null
        assert RaceCurseTalent.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/raceCurseTalent/list'

        populateValidParams(params)
        def raceCurseTalent = new RaceCurseTalent(params)

        assert raceCurseTalent.save() != null

        params.id = raceCurseTalent.id

        def model = controller.show()

        assert model.raceCurseTalentInstance == raceCurseTalent
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/raceCurseTalent/list'

        populateValidParams(params)
        def raceCurseTalent = new RaceCurseTalent(params)

        assert raceCurseTalent.save() != null

        params.id = raceCurseTalent.id

        def model = controller.edit()

        assert model.raceCurseTalentInstance == raceCurseTalent
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/raceCurseTalent/list'

        response.reset()

        populateValidParams(params)
        def raceCurseTalent = new RaceCurseTalent(params)

        assert raceCurseTalent.save() != null

        // test invalid parameters in update
        params.id = raceCurseTalent.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/raceCurseTalent/edit"
        assert model.raceCurseTalentInstance != null

        raceCurseTalent.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/raceCurseTalent/show/$raceCurseTalent.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        raceCurseTalent.clearErrors()

        populateValidParams(params)
        params.id = raceCurseTalent.id
        params.version = -1
        controller.update()

        assert view == "/raceCurseTalent/edit"
        assert model.raceCurseTalentInstance != null
        assert model.raceCurseTalentInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/raceCurseTalent/list'

        response.reset()

        populateValidParams(params)
        def raceCurseTalent = new RaceCurseTalent(params)

        assert raceCurseTalent.save() != null
        assert RaceCurseTalent.count() == 1

        params.id = raceCurseTalent.id

        controller.delete()

        assert RaceCurseTalent.count() == 0
        assert RaceCurseTalent.get(raceCurseTalent.id) == null
        assert response.redirectedUrl == '/raceCurseTalent/list'
    }
}
